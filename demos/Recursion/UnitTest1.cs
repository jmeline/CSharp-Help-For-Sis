using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Recursion
{
    public class UnitTest1
    {
        public int NotRecursiveSumSolution(List<int> values)
        {
            var sum = 0;
            foreach (var value in values)
            {
                sum += value;
            }

            return sum;
        }

        public int RecursiveSumSolution(List<int> values, int sum = 0)
        {
            var enumerable = values.ToList();
            if (!enumerable.Any())
            {
                return sum;
            }

            var value = enumerable.First();
            var nextValues = enumerable.Skip(1).ToList();
            return RecursiveSumSolution(nextValues, sum + value);
        }

        public int RecursiveFactorial(int n)
        {
           // 5! = 5 * 4 * 3 * 2 * 1
            if (n == 0)
            {
                return 1;
            }

            return n * RecursiveFactorial(n - 1);
        }
        
        [Fact]
        public void Test1()
        {
            Assert.Equal(10, NotRecursiveSumSolution(new List<int> {1, 2, 3, 4}));
            Assert.Equal(10, RecursiveSumSolution(new List<int> {1, 2, 3, 4}));
            Assert.Equal(120, RecursiveFactorial(5));
            
        }

        [Fact]
        public void Test2()
        {
            try
            {
                double i = 1.0 / 0;
            }
            catch (Exception e)
            {

            }
            finally
            {
                
            }
        }
    }
}
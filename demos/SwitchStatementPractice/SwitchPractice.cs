using System;
using Xunit;

namespace SwitchStatementPractice
{
    public class SwitchPractice
    {
        public float TestSwitch(int op1, int op2, char opr)
        {
            float result;
            switch (opr)
            {
                case '+':
                    result = op1 + op2;
                    break;
                case '-':
                    result = op1 - op2;
                    break;
                case '*':
                    result = op1 * op2;
                    break;
                case '/':
                    result = (float) (op1 / (op2 * 1.0));
                    break;
                default:
                    result = 0;
                    break;
            }

            return result;
            
        }
        
        [Fact]
        public void TestAdding()
        {
            Assert.Equal(3.0,TestSwitch(1,2,'+'));
            Assert.Equal(-1.0, TestSwitch(1,2,'-'));
            Assert.Equal(2.0, TestSwitch(1, 2, '*'));
            Assert.Equal(.5, TestSwitch(1,2,'/'));
            Assert.Equal(30, TestSwitch(1,2,')'));
        }
    }
}